package hu.ektf.warningtestapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;



public class MainActivity extends WarningActivity {

    boolean warning = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Observer.AddActivity(this);
        Observer.execute();
        Button warButton = (Button)findViewById(R.id.button);
        warButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Button","Warning:"+warning);
                if(warning)
                    Observer.EndWarning();
                else
                    Observer.Warning();
                warning = !warning;
            }
        });
    }
}
