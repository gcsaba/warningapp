package hu.ektf.warningtestapp;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Csaba on 2016.02.09..
 */
public class Observer extends AsyncTask<Integer,Void,Boolean>{
    private Boolean pre = true;
    private final static ArrayList<WarningActivity> activities = new ArrayList<>();

    public static void execute(){
        new Observer().execute(10);
    }

    public static void AddActivity(WarningActivity activity){
        activities.add(activity);
    }
    public static void RemoveActivity(WarningActivity activity){
        activities.remove(activity);
    }

    public static void isNetworkAvailable(final int timeout) {
        new Thread() {
            private boolean responded = false;
            private boolean elozo = true;
            @Override
            public void run() {
                while (true) {
                    try {
                        responded = isOnline();
                    }
                    finally {
                        if (!responded) {
                            Warning();
                        } else if (responded) {
                            EndWarning();
                        }
                        elozo = responded;
                    }
                }
            }
        }.start();
    }

    private static boolean isOnline() {
        try
        {
            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(4000);
            urlc.setReadTimeout(4000);
            urlc.connect();
            //networkcode2 = urlc.getResponseCode();
            return true;
        } catch (IOException e)
        {
            Log.i("warning", "Error checking internet connection", e);
            return false;
        }
    }

    public static void Warning(){
        for (WarningActivity act:activities) {
            act.Warning();
        }
    }
    public static void EndWarning(){
        for (WarningActivity act:activities) {
            act.EndWarning();
        }
    }

    @Override
    protected Boolean doInBackground(Integer... params) {
        boolean responded = false;
        try {
            responded = isOnline();
        }
        catch (Exception e){responded = false;}
        finally {
            return responded;
        }
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (!result && pre) {
            Warning();
        } else if (result && !pre) {
            EndWarning();
        }
        pre = result.booleanValue();
        //Log.e("Elozo", pre.toString());
        Observer.execute();
    }
}
