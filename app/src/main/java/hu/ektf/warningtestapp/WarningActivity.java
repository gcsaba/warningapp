package hu.ektf.warningtestapp;

import android.content.DialogInterface;
import android.support.v4.app.BundleCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.logging.Handler;

public class WarningActivity extends AppCompatActivity {
    protected TextView warningText;
    AlertDialog alertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warning);
        warningText = (TextView)findViewById(R.id.warningText);
        alertDialog = new AlertDialog.Builder(WarningActivity.this).create();

        alertDialog.setTitle("Warning");
        alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
        alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something

                        //Dismiss once everything is OK.
                        alertDialog.dismiss();
                    }
                });
            }
        });
    }
    Boolean stop = false;
    public final void Warning() {
        Log.e("Internet", "false");
        warningText.setText("Nincs Internet");
        alertDialog.show();
    }

    public final void EndWarning(){
        Log.e("Internet", "true");
        warningText.setText("");
        alertDialog.cancel();
    }
}
